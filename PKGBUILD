# Maintainer: Dan Johansen <strit@manjaro.org>
# Maintainer: Maxime Gauduin <alucryd@archlonux.org>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgname=libva
pkgver=2.15.0
pkgrel=2.1
pkgdesc='Video Acceleration (VA) API for Linux'
arch=('x86_64' 'aarch64')
url=https://01.org/linuxmedia/vaapi
license=(MIT)
depends=(
  libdrm
  libgl
  libx11
  libxext
  libxfixes
  wayland
)
makedepends=(
  git
  libglvnd
  mesa
  meson
)
optdepends=(
  'intel-media-driver: backend for Intel GPUs (>= Broadwell)'
  'libva-vdpau-driver: backend for Nvidia and AMD GPUs'
  'libva-intel-driver: backend for Intel GPUs (<= Haswell)'
)
provides=(
  libva-drm.so
  libva-glx.so
  libva-wayland.so
  libva-x11.so
  libva.so
)
backup=(etc/libva.conf)
_tag=b095d10bf355110352e75c22e581018a7ea7de5a #^ tag 2.15.0
source=(git+https://github.com/intel/libva.git#tag=${_tag}
      332.patch
      340.patch)
sha256sums=('SKIP'
            '9b72c70554b031069096bae563c8cbead062e4d0d6a7cd982cf2a4e10c74fc22'
            'b98dc46dc4bfef5b44e96ca684913f824e76171d4f22fa55290262a5fdb6bb27')

pkgver() {
  cd libva

  git describe --tags
}

prepare() {
    cd libva
    patch -Np1 -i ../332.patch
    patch -Np1 -i ../340.patch
}

build() {
  CFLAGS+=" -DENABLE_VA_MESSAGING"  # Option missing
  arch-meson libva build
  meson compile -C build
}

package() {
  meson install -C build --destdir "${pkgdir}"
  install -Dm 644 libva/COPYING -t "${pkgdir}"/usr/share/licenses/${pkgname}

  install -Dm 644 /dev/stdin "${pkgdir}"/etc/libva.conf <<END
LIBVA_MESSAGING_LEVEL=1
END
}
